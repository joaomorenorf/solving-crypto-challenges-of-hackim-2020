import mod
import lfe
import socket
from Crypto.Util.number import *
from hashlib import sha256

g = 1
x = 1

"""
nc crypto2.ctf.nullcon.net 5000

1 = web
2 = file
"""
receive = 1

def recvall(sock, stop='\n', p=False):
    BUFF_SIZE = 4096 # 4 KiB
    data = b''
    while True:
        part = sock.recv(BUFF_SIZE)
        if p:
        	print(part)
        data += part
        if part[-1] == ord(stop):
            break
    return data

print(" Fetching cs", end='\r')
if receive == 1:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	host = 'localhost' #'127.0.0.1' #'crypto2.ctf.nullcon.net'
	port = 5000
	s.connect((host, port))
	strnumbers = recvall(s)[30:-2].split(b', ')
elif receive == 2:
	with open("serverout", 'r') as s:
		strnumbers = s.read()[1:-1].split(', ')
print("Fetched cs  ")

cs = []
for strnumber in strnumbers:
	#print(strnumber)
	try:
		if receive == 1:
			cs.append(int(strnumber.decode('ascii')))
		elif receive == 2:
			cs.append(int(strnumber))
	except:
		print("error in:")
		print(strnumber)
		exit()

response = ""

for c in cs:
	y0 = 1 #mod.Mod(g**x, lfe.p)
	y1 = c #* y0.inverse()
	if y0*y1 != c:
		print("Something wrong happened with this c:", c)
		exit()
	response+='('+str(c)+','+str(y0)+','+str(y1)+') '
response = response[:-1]+'\n'

#print(response)
#exit()
print(" Sending response",end='\r')
if receive == 1:
	s.send(response.encode('ascii'))
elif receive == 2:
	with open("serverin", 'w') as s:
		s.write(response[:-1]) #[30:-2].split(b', ')
print('Response sent    ')

print(' Receiving challenge', end='\r')
challenge = []
if receive == 1:
	challengestream = recvall(s, stop=':').split(b"\n[")[1][2:-12].split(b")), ((")
	#print(challengestream[0])
	for twotwo in challengestream:
		two = twotwo.split(b"), (")
		c0 = int(two[0].split(b", ")[1])
		c1 = (int(two[1].split(b", ")[0]), int(two[1].split(b", ")[1]))
		challenge.append((c0,c1))
elif receive == 2:
	with open("serverout", 'r') as s:
		challenge = s.read()#[1:-1].split(', ')
print('Challenge received  ')

print(' Solving challenge', end='\r')
a = []
b = []
for cc in challenge:
    if int(sha256(long_to_bytes(1)).hexdigest(), 16) ^ 1 == cc[0]:
        b.append(1)
    elif int(sha256(long_to_bytes(1)).hexdigest(), 16) == cc[0]:
        b.append(0)
    else:
        print('there is a bug on your law, b')
        exit()
    if int(sha256(long_to_bytes(cc[1][0])).hexdigest(), 16) ^ b[-1] == cc[1][1]:
        a.append(0)
    elif int(sha256(long_to_bytes(cc[1][0])).hexdigest(), 16) ^ ((b[-1] + 1) % 2) == cc[1][1]:
        a.append(1)
    else:
        print('there is a bug on your law, a')
        exit()
print('Challenge solved  ')

print(" Sending a",end='\r')
s.send((str(a)+'\n').encode('ascii'))
print("A sent    ")

recvall(s, stop=':')

print(" Sending b",end='\r')
s.send((str(b)+'\n').encode('ascii'))
print("B sent    ")

recvall(s, stop='\n', p = True)

s.close()

exit()
