My solutions to HackIM 2020 cryptography challenges.

To test my solutions you must clone the official repository and get it running first.

https://github.com/nullcon/hackim-2020/tree/master/crypto
