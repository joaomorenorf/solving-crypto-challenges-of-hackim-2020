 #!/usr/bin/env python3
from Crypto.PublicKey import RSA, ECC
import json
from hashlib import sha256
from Crypto.Cipher import AES, PKCS1_OAEP
from base64 import b64encode, b64decode
from Crypto.Signature import DSS
from Crypto.Hash import SHA256
from os.path import isfile
import string

debug = 0

if debug:
	if isfile('rsapubkey1.pem'):
		with open('rsakeypriv1.pem','r') as f:
			rsakeypriv = RSA.import_key(f.read())
	else:
		rsakeypriv = RSA.generate(2048)
		with open('rsakeypriv1.pem','wb') as f:
			f.write(rsakeypriv.export_key('PEM'))
		keypub = rsakeypriv.publickey()
		with open('rsakeypub1.pem','wb') as f:
			f.write(keypub.export_key('PEM'))

else:
	with open('rsapubkey.pem','r') as f:
		rsakeypriv = RSA.import_key(f.read())


rsacipher = PKCS1_OAEP.new(rsakeypriv)

def verify_message(messagep, nonce, aeskey, eccpubkey, signature):
	h = SHA256.new(aeskey + nonce + messagep)
	verifier = DSS.new(eccpubkey, 'fips-186-3')
	try:
		verifier.verify(h, signature)
		return True
	except ValueError:
		return False

def sign_message(messagep, nonce, aeskey, ecckey):
	h = SHA256.new(aeskey + nonce + messagep)
	signer = DSS.new(ecckey, 'fips-186-3')
	signature = signer.sign(h)
	return signature

def signECC(b, ecckey):
	messagep = b64decode(b['message'])
	nonce = b64decode(b['nonce'])
	aeskey = b64decode(b['aeskey'])
	signature = sign_message(messagep, nonce, aeskey, ecckey)
	return signature

## Parsing function had been copied from official resolution

import pwn

def get_receipt(message):
	r = pwn.remote('127.0.0.1', 5000)#"crypto1.ctf.nullcon.net", 5001) # change this to server
	r.recvuntil("Enter message in json format: ")
	r.sendline(json.dumps(message) + '\n')
	r.recvuntil("Here is your read receipt:")
	r.recvline()
	ret = r.recvline().strip().decode()
	r.close()
	return ret

##

def encrypt_message(message, aeskey):
	cipher = AES.new(aeskey, AES.MODE_CTR)
	nonce = cipher.nonce
	messagee = cipher.encrypt(message)

	messagep = rsacipher.encrypt(aeskey)
	if debug:
		print(rsacipher.decrypt(messagep))
	return messagee, nonce, messagep

def createecc():
	ecckey = ECC.generate(curve='P-256')
	eccpubkey = ecckey.public_key().export_key(format="DER")
	return ecckey, eccpubkey

def aMessage():
	messagec = b'1234567890123456' #input("Enter message: ")
	aeskey = b'password12345678'
	messagep, nonce, aeskey = encrypt_message(messagec, aeskey)

	ecckey, eccpubkey = createecc()

	signature = sign_message(messagep, nonce, aeskey, ecckey)

	if debug:
		print(verify_message(messagep, nonce, aeskey, eccpubkey, signature))

	a = {'message' : b64encode(messagep).decode(),
		'nonce' : b64encode(nonce).decode(),
		'aeskey' : b64encode(aeskey).decode(),
		'eccpubkey' : b64encode(ecckey.export_key(format='PEM').encode('ascii')).decode(),
		'signature' : b64encode(signature).decode()}

	message = json.dumps(a)
	print(message)

def bMessage(a):
	message = str()
	b=dict()
	ecckey, eccpubkey = createecc()
	b['aeskey'] = a['aeskey']
	b['nonce'] = a['nonce']
	b['eccpubkey'] = b64encode(eccpubkey).decode()
	for ii in range(len(b64decode(a['message']))):
		b['message'] = b64encode(b64decode(a['message'])[:ii+1]).decode()
		b['signature'] = b64encode(signECC(b, ecckey)).decode()
		hashReceived = get_receipt(b)
		for jj in string.printable:
			hashGuessed = SHA256.new((message + jj).encode('ascii')).hexdigest()
			if hashReceived == hashGuessed:
				message += jj
				print(message)
				break
	return message

intmessage = {"aeskey": "nwmHkXTN/EjnoO5IzhpNwE3nXEUMHsNWFI7dcHnpxIIiXCO+dLCjR6TfqYfbL9Z6a7SNCKbeTFBLnipXcRoN6o56urZMWwCioVTsV7PHrlCU42cKX+c/ShcVFrA5aOTTjaO9rxTMxB1PxJqYyxlpNaUpRFslzj9LKH+g8hVEuP9lVMm7q4aniyOUgPrAxyn044mbuxPu6Kh+JHSt5dkmnPZGNfUDKCwvMKeilb5ZkLaW/EaoXXsJLh/wUinMROIqmD2dkiWnk10633sJIu1lEOUsiykYXtJcd3o/B2dfTx2/85C2J6IsIp3+jJne76AYryAONPSxuh+M0h1xCzNeQg==", "message": "6VCnnSOU1DBImyhlqt7SoEjRtmBxjmABFVmXYhlKDyc+NBlnZ3Hpj4EkLwydPGpHiAvr4R0zTXSyUnMk5N6fi0/BFZE=", "nonce": "Cems9uHF6mk=", "signature": "uhLCnBvGfdC1fVkGUKQ8zNp/fOXNnFxNuDEc7CDGEYSxnuZMoGqbEqMLguJqDdvHFSHoUrq2R9/+mfk8LHndhw==", "eccpubkey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEGww+NA3xHj4kCyztekLhmJVB62Hhq/oGDWwo4fxgZCgbODqD3vrMFFTGCWfO8ZyHtstuW+Yztpq94CnSNpJoug=="}

if __name__ == '__main__':
	#aMessage()
	bMessage(intmessage)
